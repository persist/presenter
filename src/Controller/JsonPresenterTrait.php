<?php declare(strict_types=1);

namespace Persist\PresenterBundle\Controller;

use Persist\PresenterBundle\Presenter\Presenter;
use Symfony\Component\HttpFoundation\JsonResponse;

trait JsonPresenterTrait
{
    /**
     * @inheritDoc
     */
    protected function present(Presenter $presenter, array $headers = []): JsonResponse
    {
        return new JsonResponse($presenter->getResponse(), $presenter->getStatusCode(), $headers);
    }
}

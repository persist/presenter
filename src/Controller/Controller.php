<?php declare(strict_types=1);

namespace Persist\PresenterBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class Controller extends AbstractController
{
    use JsonPresenterTrait;
}

<?php declare(strict_types=1);

namespace Persist\PresenterBundle\Presenter;

use Persist\BusBundle\CommandBus\CommandBusInterface;
use Persist\BusBundle\CommandBus\CommandBusAwareTrait;

class CommandPresenter extends Presenter
{
    use CommandBusAwareTrait;

    /**
     * @param CommandBusInterface $commandBus
     */
    public function __construct(CommandBusInterface $commandBus)
    {
        $this->setCommandBus($commandBus);
    }

    /**
     * @inheritDoc
     */
    protected function handleAction($object)
    {
        $this->handleCommand($object);
    }
}

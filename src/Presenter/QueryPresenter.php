<?php declare(strict_types=1);

namespace Persist\PresenterBundle\Presenter;

use Persist\BusBundle\QueryBus\QueryBusInterface;
use Persist\BusBundle\QueryBus\QueryBusAwareTrait;

class QueryPresenter extends Presenter
{
    use QueryBusAwareTrait;

    /**
     * @param QueryBusInterface $queryBus
     */
    public function __construct(QueryBusInterface $queryBus)
    {
        $this->setQueryBus($queryBus);
    }

    /**
     * @inheritDoc
     */
    protected function handleAction($object)
    {
        return $this->handleQuery($object);
    }

    /**
     * @inheritDoc
     */
    protected function getResult(): array
    {
        return ! $this->hasException() ? $this->getOutput() : [];
    }
}

<?php declare(strict_types=1);

namespace Persist\PresenterBundle\Presenter;

abstract class Presenter
{
    /** @var mixed */
    protected $output;

    /** @var \Throwable */
    protected $exception;

    abstract protected function handleAction($object);

    public function handle($command): void
    {
        try {
            $output = $this->handleAction($command);
            $this->validate($output);
            $this->output = $output;
        } catch (\Throwable $exception) {
            $this->exception = $exception;
            $this->exceptionCallback($exception);
        }
    }

    protected function validate($output): void
    {
    }

    protected function exceptionCallback(\Throwable $exception): void
    {
    }

    public function hasException(): bool
    {
        return $this->exception !== null;
    }

    public function getException(): \Throwable
    {
        return $this->exception;
    }

    public function getResponse(): array
    {
        return [
            'meta' => $this->getMeta(),
            'result' => $this->getResult(),
        ];
    }

    protected function getMeta(): array
    {
        return [
            'status' => $this->getStatusCode(),
            'message' => $this->hasException() ? $this->getExceptionMessage() : null,
        ];
    }

    public function getStatusCode(): int
    {
        return $this->hasException() ? $this->getErrorStatusCode() : $this->getSuccessStatusCode();
    }

    protected function getSuccessStatusCode(): int
    {
        return 200;
    }

    protected function getErrorStatusCode(): int
    {
        return 400;
    }

    public function getExceptionMessage(): string
    {
        return $this->getException()->getMessage();
    }

    protected function getResult(): array
    {
        return [];
    }

    public function getOutput()
    {
        return $this->output;
    }

    protected function hasOutput(): bool
    {
        return $this->output !== null;
    }
}

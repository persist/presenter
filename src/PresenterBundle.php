<?php declare(strict_types=1);

namespace Persist\PresenterBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PresenterBundle extends Bundle
{
}
